const mongoose = require('../../database/index')

const TransactionSchema = new mongoose.Schema({
    coin: {
        type: String,
        required: true
    },
    typeTrans: {
        type: String,
        required: true
    },
    amount: {
        type: Number,
        required: true
    },
    confirmed: {
        type: Boolean,
        default: false
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    createdAt: {
        type: Date,
        default: Date.now()
    }
})

const Transaction = mongoose.model('Transaction', TransactionSchema)

module.exports = Transaction