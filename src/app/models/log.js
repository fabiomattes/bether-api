const mongoose = require('../../database/index')

const LogSchema = new mongoose.Schema({
    acao: {
        type: String,
        required: true
    },
    ip: {
        type: String,
        required: true
    },
    createdAt: {
        type: Date,
        default: Date.now()
    }
})

const Log = mongoose.model('Log', LogSchema)

module.exports = Log