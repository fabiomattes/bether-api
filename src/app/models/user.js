const mongoose = require("../../database/index");
const bcrypt = require("bcryptjs");
const Speakeasy = require("speakeasy");
const QRCode = require("qrcode");

const UserSchema = new mongoose.Schema({
  nome_completo: {
    type: String,
    required: true
  },
  email: {
    type: String,
    unique: true,
    required: true,
    lowercase: true
  },
  senha: {
    type: String,
    required: true,
    select: false
  },
  admin: {
    type: Boolean,
    required: true,
    default: false
  },
  tokenResetSenha: {
    type: String,
    select: false
  },
  tokenResetExpires: {
    type: Date,
    select: false
  },
  secretOtp: {
    type: String,
    select: false
  },
  qrUrlOtp: {
    type: String,
    select: false
  },
  createdAt: {
    type: Date,
    default: Date.now()
  }
});

UserSchema.pre("save", async function(next) {
  // const secret = Speakeasy.generateSecret({ length: 16, name: 'Betherpay' })
  // this.secretOtp = secret.base32

  // this.qrUrlOtp = secret.otpauth_url

  const hash = await bcrypt.hash(this.senha, 10);
  this.senha = hash;

  next();
});

const User = mongoose.model("User", UserSchema);

module.exports = User;
