const mongoose = require('../../database/index')

const BalanceSchema = new mongoose.Schema({
    brl: {
        type: Number,
        default: 0.00
    },
    brlt: {
        type: Number,
        default: 0.00
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    createdAt: {
        type: Date,
        default: Date.now()
    }
})

const Balance = mongoose.model('Balance', BalanceSchema)

module.exports = Balance