const express = require("express");
const router = express.Router();
const authMiddleware = require("../middlewares/auth");
const Balance = require("../models/balance");
const Log = require("../models/log");
const Transaction = require("../models/transaction");

router.use(authMiddleware);

router.get("/", async (req, res) => {
  try {
    const balances = await Balance.find().populate("user");

    return res.send({ balances });
  } catch (error) {
    return res.status(200).send({ error: true, description: error });
  }
});

router.get("/:userId", async (req, res) => {
  const user = req.params.userId;
  try {
    const balances = await Balance.findOne({ user });

    return res.send({ balances });
  } catch (error) {
    return res.status(200).send({ error: true, description: error });
  }
});

router.post("/", async (req, res) => {
  try {
    await Balance.create({ ...req.body, user: req.userId });

    return res.send({ error: false, msg: "Balanços gerados" });
  } catch (error) {
    return res.status(200).send({ error: true, description: error });
  }
});

router.put("/:balanceId", async (req, res) => {
  try {
    const balance = await Balance.findByIdAndUpdate(
      req.params.balanceId,
      req.body,
      { new: true }
    );
    return res.send({ error: false, balance });
  } catch (error) {
    return res.status(200).send({ error: true, description: error });
  }
});

router.delete("/:balanceId", async (req, res) => {
  const user = req.params.balanceId;
  try {
    await Balance.findByIdAndRemove(user);

    return res.send();
  } catch (error) {
    return res.status(200).send({ error: true, description: error });
  }
});

module.exports = app => app.use("/balances", router);
