const express = require("express");
const User = require("../models/user");
const Log = require("../models/log");
const router = express.Router();
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const crypto = require("crypto");
const authConfig = require("../../config/auth");
const mailer = require("../../modules/mailer");
const Balance = require("../models/balance");
const Speakeasy = require("speakeasy");
const QRCode = require("qrcode");

var getClientIp = function(req) {
  return (
    (
      req.headers["X-Forwarded-For"] ||
      req.headers["x-forwarded-for"] ||
      ""
    ).split(",")[0] || req.client.remoteAddress
  );
};

router.post("/registro", async (req, res) => {
  const { email } = req.body;

  try {
    if (await User.findOne({ email })) {
      await Log.create({
        acao: `Usuário ${email} tentou se cadastrar novamente no sistema`,
        ip: getClientIp(req)
      });

      return res
        .status(200)
        .send({ error: true, msg: "Usuário já cadastrado" });
    }

    await Log.create({
      acao: `Usuário ${email} se cadastrou no sistema`,
      ip: getClientIp(req)
    });

    const secret = Speakeasy.generateSecret({ length: 16, name: "Betherpay" });
    const secretOtp = secret.base32;

    const qrUrlOtp = secret.otpauth_url;

    const user = await User.create({
      nome_completo: req.body.nome_completo,
      email: req.body.email,
      senha: req.body.senha,
      secretOtp,
      qrUrlOtp
    });

    user.senha = undefined;

    await Balance.create({ user: user._id });

    return res.send({ user });
  } catch (e) {
    return res.status(200).send({ error: "Falha no registro", descricao: e });
  }
});

router.post("/login", async (req, res) => {
  const { email, senha } = req.body;

  const user = await User.findOne({ email }).select(
    "+senha nome_completo secretOtp"
  );

  if (!user) {
    await Log.create({
      acao: `Tentativa de acesso com o usuário ${email} no sistema`,
      ip: getClientIp(req)
    });

    return res.status(200).send({ error: true, msg: "Email inválido" });
  }

  if (!(await bcrypt.compare(senha, user.senha))) {
    await Log.create({
      acao: `Tentativa de acesso com o usuário ${email} no sistema usando a senha ${senha}`,
      ip: getClientIp(req)
    });

    return res.status(200).send({ error: true, msg: "Senha inválida" });
  }

  await Log.create({
    acao: `Usuário ${email} acessou o sistema`,
    ip: getClientIp(req)
  });

  user.senha = undefined;

  const token = jwt.sign(
    {
      id: user.id,
      nome: user.nome_completo,
      secretOtp: user.secretOtp
    },
    authConfig.secret,
    {
      expiresIn: 86400
    }
  );

  res.send({ user, token });
});

router.post("/forgot_password", async (req, res) => {
  const { email } = req.body;

  try {
    const user = await User.findOne({ email });

    if (!user) {
      await Log.create({
        acao: `Tentativa de recuperação de senha com o usuário ${email} no sistema`,
        ip: getClientIp(req)
      });

      return res.status(200).send({ error: true, msg: "Email inválido" });
    }

    const token = crypto.randomBytes(20).toString("hex");
    const now = new Date();
    now.setHours(now.getHours() + 1);

    await User.findByIdAndUpdate(user.id, {
      $set: {
        tokenResetSenha: token,
        tokenResetExpires: now
      }
    });

    mailer.sendMail(
      {
        to: email,
        from: "opentisolucoes@gmail.com",
        template: "auth/forgot_password",
        context: { token }
      },
      err => {
        if (err) return res.status(200).send({ error: true, msg: err });

        return res.send({
          error: false,
          msg: "Foi enviado um email com informações para alteração de senha"
        });
      }
    );

    await Log.create({
      acao: `Usuário ${email} solicitou alteração de senha`,
      ip: getClientIp(req)
    });
  } catch (e) {
    return res.status(200).send({ error: "Ocorreu um erro", descricao: e });
  }
});

router.post("/reset_password", async (req, res) => {
  const { email, token, senha } = req.body;

  try {
    const user = await User.findOne({ email }).select(
      "+tokenResetSenha tokenResetExpires"
    );

    if (!user) {
      await Log.create({
        acao: `Tentativa de reset de senha com o usuário ${email} no sistema`,
        ip: getClientIp(req)
      });

      return res.status(200).send({ error: true, msg: "Email inválido" });
    }

    if (token !== user.tokenResetSenha) {
      await Log.create({
        acao: `Tentativa de reset de senha com o token ${token} no sistema`,
        ip: getClientIp(req)
      });

      return res.status(200).send({ error: true, msg: "Token inválido" });
    }

    const now = new Date();

    if (now > user.tokenResetExpires) {
      await Log.create({
        acao: `Tentativa de reset de senha com o token ${token} no sistema, token expirado`,
        ip: getClientIp(req)
      });

      return res.status(200).send({ error: true, msg: "Token expirado" });
    }

    user.senha = senha;

    await user.save();

    await Log.create({
      acao: `Usuário ${email} alterou a senha`,
      ip: getClientIp(req)
    });

    return res.send({
      error: false,
      msg: "Senha alterada com sucesso, redirecionando para o login!"
    });
  } catch (err) {
    return res.status(200).send({ error: true, descrition: err });
  }
});

module.exports = app => app.use("/auth", router);
