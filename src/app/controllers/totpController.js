const express = require("express");
const User = require("../models/user");
const Log = require("../models/log");
const router = express.Router();
const Speakeasy = require("speakeasy");
const authMiddleware = require("../middlewares/auth");

var getClientIp = function(req) {
  return (
    (
      req.headers["X-Forwarded-For"] ||
      req.headers["x-forwarded-for"] ||
      ""
    ).split(",")[0] || req.client.remoteAddress
  );
};

router.post("/validate", async (req, res) => {
  const { secret, token } = req.body;

  const valido = Speakeasy.totp.verify({
    secret: secret,
    encoding: "base32",
    token: token,
    window: 0
  });

  if (!valido) {
    await Log.create({
      acao: `Tentativa de acesso com token inválido no 2fa do sistema`,
      ip: getClientIp(req)
    });

    return res.status(200).send({ error: true, msg: "2fa inválido" });
  }

  res.send({ error: false, msg: "2fa Valido, redirecionando" });
});

module.exports = app => app.use("/totp", router);
