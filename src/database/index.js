const mongoose = require("mongoose");

mongoose.connect(
  "mongodb+srv://betherpay:betherpay2019@cluster0-jtlbt.mongodb.net/betherpay?retryWrites=true",
  { useNewUrlParser: true }
);
mongoose.Promise = global.Promise;

module.exports = mongoose;
