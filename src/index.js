const express = require("express");
const bodyParser = require("body-parser");
var cors = require("cors");

const app = express();

var corsOptions = {
  origin: "*"
};

app.use(cors(corsOptions));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

require("./app/controllers/index")(app);

app.listen(3003, () => console.info("Running on localhost:3003"));
